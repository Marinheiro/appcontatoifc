package model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class Contato implements Serializable {

    private String room;
    private String role;
    private String department;
    private String mail;
    private String phone;
    private String ria;
    private String pdt;
    private String qad;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        if (!(this.phone).equals("-")) {
            String[] lista_phone = this.phone.split(Pattern.quote("/"));
            String format_phone = "";
            for (String elemento : lista_phone) {
                format_phone = format_phone + "3803-" + elemento + "\n";
            }
            return format_phone;
        } else {
            return "---- ----";
        }
    }

    public String getFirstPhone() {
        if (!(this.phone).equals("-")) {
            String[] lista_phone = this.phone.split(Pattern.quote("/"));
            return "3803-" + lista_phone[0];
        } else {
            return "---- ----";
        }
    }

    public List<String> getListPhone() {
        List<String> phones = new LinkedList<String>();
        if (!(this.phone).equals("-")) {
            String[] lista_phone = this.phone.split(Pattern.quote("/"));
            for (String elemento : lista_phone) {
                phones.add("3803-" + elemento);
            }
            return phones;
        } else {
            return phones;
        }
    }

    public List<String> getListMail() {
        List<String> list_mail = new LinkedList<String>();
        if (!(this.mail).equals("-")) {
            List<String> lista_mail = Arrays.asList(this.phone.split(Pattern.quote("ou")));
            list_mail.addAll(lista_mail);
            return list_mail;
        } else {
            return list_mail;
        }
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRia() {
        return ria;
    }

    public void setRia(String ria) {
        this.ria = ria;
    }

    public String getPdt() {
        return pdt;
    }

    public void setPdt(String pdt) {
        this.pdt = pdt;
    }

    public String getQad() {
        return qad;
    }

    public void setQad(String qad) {
        this.qad = qad;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getInfos() {
        return this.getName()
                + "\nDepartamento: " +
                this.getDepartment()
                + "\nCargo: " +
                this.getRole()
                + "\nSala: " +
                this.getRoom()
                + "\nE-mail:\n" +
                this.getMail()
                + "\nTelefone:\n" +
                this.getPhone();
    }

}
