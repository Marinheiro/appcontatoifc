package service;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import model.Contato;

public class ContatoService extends AsyncTask<Void, Void, List<Contato>> {

    private String name_search;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.i("AST", "onPreExecute");
    }

    @Override
    protected void onPostExecute(List<Contato> contato) {
        super.onPostExecute(contato);
        Log.i("AST", "onPostExecute");
    }

    @Override
    protected List<Contato> doInBackground(Void... voids) {
        Log.i("AST", "doInBackground");
        StringBuilder resposta = new StringBuilder();
        try {
            //URL url = new URL("http://dev-apihorarios.fabricadesoftware.ifc.edu.br/busca-servidor/" + this.name_search);
            URL url = new URL("http://dev-apihorarios.fabricadesoftware.ifc.edu.br/lista-servidores/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
                resposta.append(scanner.next());
                resposta.append(" ");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Type listType = new TypeToken<List<Contato>>() {
        }.getType();
        return new Gson().<List<Contato>>fromJson(resposta.toString(), listType);
    }
}
