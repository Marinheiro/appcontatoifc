package com.thiago.iago.contatoifc;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import model.Contato;

public class ResultadoActivity extends AppCompatActivity {
    TextView tvResultadoNome, tvResultadoEmail, tvResultadoDepartamento, tvResultadoPhone, tvResultadoRoom;
    Button butCompartilhar;
    Contato contato;
    ListView lvRamais;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        this.tvResultadoNome = findViewById(R.id.tv_resultado_nome);
        this.tvResultadoEmail = findViewById(R.id.tv_resultado_email);
//        this.tvResultadoPhone = findViewById(R.id.tv_resultado_phone);
        this.tvResultadoRoom = findViewById(R.id.tv_resultado_room);
        this.tvResultadoDepartamento = findViewById(R.id.tv_resultado_departamento);
        this.lvRamais = findViewById(R.id.lv_ramais);
        this.butCompartilhar = findViewById(R.id.btn_resultado_compartilhar);

        //obter a intent que iniciou esta activity
        Intent intent = this.getIntent();
        this.contato = (Contato) intent.getSerializableExtra("Contato");
        this.tvResultadoNome.setText(contato.getName());
        this.tvResultadoEmail.setText(contato.getMail());
//        this.tvResultadoPhone.setText(contato.getPhone());
        this.tvResultadoRoom.setText(contato.getRoom());
        this.tvResultadoDepartamento.setText(contato.getDepartment());
        this.populaRamais();
    }


    public void populaRamais() {

        final List<String> ramais = contato.getListPhone();
        final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, ramais);
        lvRamais.setAdapter(adapter);
        lvRamais.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String ramal_call = (String) adapter.getItem((int) i);
                Uri uri = Uri.parse("tel:" + ramal_call);
                Intent intentCompartilhar = new Intent(Intent.ACTION_DIAL, uri);
                startActivity(intentCompartilhar);

            }
        });
    }

    public void compartilhar(View view) {
        Intent intentCompartilhar = new Intent(Intent.ACTION_SEND);
        intentCompartilhar.setType("text/plain");
        intentCompartilhar.putExtra(Intent.EXTRA_TEXT, this.contato.getInfos());
        startActivity(intentCompartilhar);
    }

    public void enviar(View view) {
        Uri uri = Uri.parse("mailto:" + this.tvResultadoEmail.getText());
        Intent intentCompartilhar = new Intent(Intent.ACTION_SEND);
        intentCompartilhar.setData(uri);
        intentCompartilhar.setType("text/plain");
        intentCompartilhar.putExtra(Intent.EXTRA_EMAIL, this.tvResultadoEmail.getText());
        startActivity(intentCompartilhar);
    }

    public void ligar(View view) {
        Uri uri = Uri.parse("tel:" + this.contato.getFirstPhone());
        Intent intentCompartilhar = new Intent(Intent.ACTION_DIAL, uri);
        startActivity(intentCompartilhar);

    }
}
