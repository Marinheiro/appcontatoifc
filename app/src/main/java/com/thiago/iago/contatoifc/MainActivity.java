package com.thiago.iago.contatoifc;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

import model.Contato;
import service.ContatoService;

public class MainActivity extends AppCompatActivity {
    EditText edtContato;
    ImageButton btnConsulta;
    ListView lvContato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.edtContato = findViewById(R.id.edt_consulta_contato);
        this.btnConsulta = findViewById(R.id.btn_consulta);
        this.lvContato = findViewById(R.id.lv_main_contato);
        buscarContato();
    }

    public void buscarContato() {
        try {
            List<Contato> contatoRetorno = new ContatoService().execute().get();
            final List<Contato> dadosContato = contatoRetorno;
            final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, dadosContato);
            lvContato.setAdapter(adapter);
            lvContato.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                Contato contatoDetalhe;

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    this.contatoDetalhe = (Contato) adapter.getItem((int) i);
                    Intent intent = new Intent(MainActivity.this, ResultadoActivity.class);
                    intent.putExtra("Contato", contatoDetalhe);
                    startActivity(intent);
                }
            });
            btnConsulta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String contato = edtContato.getText().toString();
                    adapter.getFilter().filter(contato);
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
